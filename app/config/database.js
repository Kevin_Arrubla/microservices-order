const MAX_CONNECTION_POOLSIZE = 5;

const {
DB_NAME = 'orders',
DB_USER = 'postgres',
DB_PASS = '0058',
DB_HOST = 'localhost',
DB_PORT = 5432,
} = process.env;

module.exports = {
    client: 'pg',
    connection: `postgress://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}`,
    pool: {min:1, max: MAX_CONNECTION_POOLSIZE  },
    acquireConnectionTimeout: 5000,
}