const OrdersControllers = module.exports;
const OrdersService = require('../services/OrdersSerices');
const { BaseError } = require('../utils/ErrorHandlerMiddleware');
const log4j = require('../utils/logger');
const LogUtils = require('../utils/LogUtils');
const Validator = require('../validators/Validator')
const OrderRegisterSchema = require('../validators/OrderRegisterSchema');

/**
 * @api {POST} /api/orders
 * @apiName RegisterOrder
 * @apiGroup Order
 * @apiDescription  Register a Order of a device
 *
 * @apiParam (body) {Object} Order value
 * @apiParamExample {json} Body example:
 * {
 *    device_id:423423,
 *    value: 12.3,
 * }
 *
 * @apiSuccessExample Success Response:
 * HTTP/1.1 200
 *
 * @apiError (400) {null} Error if object param is invalid
 * @apiError (500) {Object} Error on internal runtime, should return nothing.
 */
OrdersControllers.save = async (req, res, next) => {
    const logName = 'SaveOrder';
    const logger = LogUtils.getLoggerWithId(log4j,logName);
    const {body} = req;

    try
    {
        Validator(OrderRegisterSchema).validateRequest(body);
     
        logger.info(`Starts OrdersController.save: params ${JSON.stringify(body)}`);
        
        return OrdersService.create(body, { logger, logName})
            .then(respose => res.send(respose))
            .catch(error => next(new BaseError(error.message)));
           
    }catch (error){
        return next(error);
    }

};

/**
 * @api {GET} /orders/getOne
 * @apiName Search Rrder
 * @apiGroup Order
 * @apiDescription  Search a Order of a order
 *
 * @apiParam (body) {Params} Order value
 * @apiParamExample {json} Body example:
 * {
 *    device_id:423423,
 *    value: 12.3,
 * }
 *
 * @apiSuccessExample Success Response:
 * HTTP/1.1 200
 *
 * @apiError (400) {null} Error if object param is invalid
 * @apiError (500) {Object} Error on internal runtime, should return nothing.
 */

OrdersControllers.getOne = async(req, res, next) => {
    const logName = 'Search Order';
    const logger = LogUtils.getLoggerWithId(log4j,logName);
    const idUsers=req.params
    logger.info(`Starts OrdersController.getOne params: ${JSON.stringify(idUsers)}`);

        return OrdersService.getOne(idUsers, {logger,logName})
        .then(response=> res.send(response))
        .catch(error => next(new BaseError(error.message)))

}

OrdersControllers.getOrders = async(req, res, next) => {
    const logName = "getOrders";
    const logger = LogUtils.getLoggerWithId(log4j,logName);
    const {body} = req;
    

    return OrdersService.getOrder()
    .then(response => res.send(response))
    .catch(error => next(new BaseError(error.message)))
}