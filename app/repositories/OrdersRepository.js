const OrdersRepository = module.exports;
const DB = require('../utils/DB');

OrdersRepository.create = (order) => DB('orders').insert(order);

OrdersRepository.getOne = idUsers => DB('orders').select('*').where( idUsers );

OrdersRepository.getOrder = () => DB('orders').select('*').from('orders');
