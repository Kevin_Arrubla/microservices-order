const express = require('express');
const OrdersController = require('./controllers/OrdersControllers');


const router = express.Router();

router.post('/save',OrdersController.save );
router.get('/getOne/:id',OrdersController.getOne);
router.get('/getOrders', OrdersController.getOrders);

module.exports = router;