const OrdersServices = module.exports;
const OrdersRepository = require('../repositories/OrdersRepository');
const log4js = require('../utils/logger');

const defaultLogger = log4js.getLogger('OrdersService');


OrdersServices.create = async (orders, options = {}) => {

    const {logger = defaultLogger } = options;
    logger.info(`OrdersService.create with ${JSON.stringify(orders)}`);

    const {id_users:idUsers,date_order:dateOrder,address:Address,id_plate:idPlate } = orders;

    return OrdersRepository.create({id_users:idUsers,date_order:dateOrder,address:Address,id_plate:idPlate})
}


OrdersServices.getOne = async (idUsers, options = {}) =>{
    const {logger = defaultLogger} = options;
    logger.info(`OrdersServices.getOne with ${JSON.stringify(idUsers)}`);

    return OrdersRepository.getOne(idUsers);
}

OrdersServices.getOrder = async (orders, options = {}) =>{
    const {logger = defaultLogger} = options;
    logger.info(`OrdersService.getOrders with ${JSON.stringify(orders)}`);

    return OrdersRepository.getOrders()
}