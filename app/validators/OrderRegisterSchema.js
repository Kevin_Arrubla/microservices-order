module.exports = {
    title: 'OrdersRegister',
    type: 'object',
    properties:{
        id_users:{
            type: 'number',
        },
        date_order:{
            type:  'string',
        },
        address:{
            type: 'string',
        },
        id_plate:{
            type: 'number',
        },
    }
};