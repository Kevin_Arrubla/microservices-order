const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./app/routes');
const ErrorHandlerMiddleware = require('./app/utils/ErrorHandlerMiddleware');
const log4js = require('log4js');

const app = express();
const {PORT = 3000} = process.env;

app.use(bodyParser.json());

const logger = log4js.getLogger('cpgops-orders-ms');

process.on('unhandledRejection', (reason,p) =>{
    logger.error('unhandled Rejection at: Promise', p , 'resaon: ', reason);
    logger.error(reason.stack);
});

app.use('/orders', routes);
app.use(ErrorHandlerMiddleware.MainHandler);

app.listen(PORT, () =>{
    console.log('escuchando el puerto ',PORT);
    
});

module.exports = app;